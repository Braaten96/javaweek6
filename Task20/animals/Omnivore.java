package animals;
import java.util.*;

public class Omnivore extends Animal {

	public Omnivore(String name, ArrayList<Moveable> moves){
		super(name, moves);
	}

	public String type() {
		return " is a Omnivore";
	}

}
