package animals;

public class Fly implements Moveable{
	
	public String getMoveName(){
		return "Fly"; 
	}

	public String doing(){
		return "Flying"; 
	}

}