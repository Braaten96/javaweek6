package animals;
import java.util.*;

public class Monkey extends Herbivore{

	public Monkey(String name, ArrayList<Moveable> moves){
		super(name, moves);
	}

	public String info(){
		return super.getName() + super.type() + " and can: " + getMove() 
		+ "\n" + super.getName() + " is " + getRandom();
	}

	public String getMove() {
		return super.moveName();
	}

	public String getRandom() {
		return super.doingRandom();
	}


}