package animals;

public interface Moveable {
	String getMoveName();
	String doing();
}