import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;

class PetRockTest {
    private PetRock rocky;

    @BeforeEach
    public void myTestSetUp(){
        rocky = new PetRock("Rocky");
    }
    @Test
    void getName() {
        assertEquals("Rocky", rocky.getName());
    }

    @Test
    void testUnhappyToStart() {
        assertFalse(rocky.isHappy());
    }

    @Test
    void testHappyAfterPlay() {
        rocky.playWithRock();
        assertTrue(rocky.isHappy());
    }

    @Disabled ("Exception throwing now yet defined")
    @Test ()
    void nameFail(){
        Assertions.assertThrows(IllegalStateException.class, () -> rocky.getHappyMessage());
    }

    @Test ()
    void name(){
        rocky.playWithRock();
        String msg = rocky.getHappyMessage();
        assertEquals("I'm happy!", msg);
    }

    @Test
    void testFavNum() {
        assertEquals(42, rocky.getFavNumber());
    }

    @Test
    void emptyNameFail() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> new PetRock(""));
    }
}